package com.wpen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@SpringBootApplication
@EnableDiscoveryClient // 开启服务发现客户端，加载Nacos配置中心配置项
@EnableConfigurationProperties
@RefreshScope   // 配置项动态生效
public class CloudParamApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudParamApplication.class, args);
    }

}
