package com.wpen.web;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class UserDto {

    @NotNull
    private String name;
    private String custNo;
    private int age;
    private String work;
    private String addr;
    
}
