package com.wpen.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
@RequestMapping("/api")
public class ApiController {
    @Value("${spring.application.name}")
    private String appName;
    @Value("${wpen.app.version}")
    private String version;

    @GetMapping("/get-name")
    public String kvAdd() {
        return appName + ";" + version;
    }

    @GetMapping(value = "/get-username" , produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto getName(@RequestParam String name) {
        UserDto userDto = new UserDto();
        userDto.setName(name);
        userDto.setCustNo("no customer username");
        userDto.setAddr(version);
        return userDto;
    }

    @PostMapping(value = "/get-userinfo", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto getUserInfo(@RequestBody @Valid UserDto userDto) {
        userDto.setCustNo("no customer userinfo");
        userDto.setAddr(appName);
        return userDto;
    }
}
